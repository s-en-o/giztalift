<?php
	
include('../config.php');

class DB{
	
private $db;

//	connect to DB
public function __construct(){
	
	try{
		
		$this->db = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
		
		if(mysqli_connect_errno()){
			
			throw new Exception("Unable Connect to DB.");
			
		}# end if
		
	}# end try
	
	catch (Exception $e){
		
		die($e-> getMessage());
		
	}# end catch
	
}# end __construct

// get all the page info from DB 
// in the view-home
public function pageInfo($page){

	$qry = "SELECT pageName, pageTitle, pageHeading, pageContent, pageDescription FROM pages WHERE pageName = '$page'";
	$rs = $this-> db-> query($qry);
	if($rs){
		
		if($rs-> num_rows > 0){
			
			$pageInfo = $rs-> fetch_assoc();
			return $pageInfo;
			
		}else{
			
			// echo('This page does not exists.');
			
		}# end if
		
	}else{
			
			echo('Error executing query.') . $qry;
			
	}# end if
		
}# end pageInfo

// get list of all users Ascending
// in the view-home
public function showUserAsc(){

	$qry = "SELECT postID, firstName, lastName, profilePic, suburbName, timeDeparture, postDescription, carSpace, timeStamp FROM usersPost, users, suburbs WHERE usersPost.userID = users.userID AND suburbs.suburbID = usersPost.suburbID ORDER BY timeStamp DESC LIMIT 0, 5 ";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$usersAsc = array();

			while($row = $rs-> fetch_assoc()){

				$usersAsc[] = $row;

			}# end if

			return $usersAsc;

		}else{

			echo ('The user does not exists.');

		}# end if

	}else{

		echo ('Error executing query.') . $qry;

	}# end if

}# end getUser

// match the input username and password with data in DB
public function getUser(){

	extract($_POST);
	$password = sha1($userPassword);
	$qry = "SELECT userID, username, firstname, lastname, userAccess FROM users WHERE username = '$userName' AND password = '$password'";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$user = $rs-> fetch_assoc();
			return $user;
	
		}# end if

	}else{

		echo "Error executing query". $qry;

	}# end if

}# end getUser

// get the user info from DB
public function getUserByID($userID){

	$qry = "SELECT username, firstname, lastname FROM users WHERE userID = $userID ";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$user = $rs-> fetch_assoc();
			return $user;
	
		}# end if

	}else{

		// echo "Error executing query". $qry;

	}# end if

}# end getUser

public function getAdmin(){

	extract($_POST);
	$password = sha1($userPassword);
	$qry = "SELECT username, firstname, lastname, userAccess FROM users WHERE username = '$userName' AND password = '$password'";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$user = $rs-> fetch_assoc();
			return $user;
	
		}# end if

	}else{

		echo "Error executing query". $qry;

	}# end if

}# end getAdmin

// get the single post
// view-post
function postByID($postID){

$qry = "SELECT usersPost.userID, firstName, lastName, suburbName, timeDeparture, postDescription, carSpace, timeStamp FROM usersPost, users, suburbs WHERE usersPost.userID = users.userID AND suburbs.suburbID = usersPost.suburbID AND postID = $postID LIMIT 0, 5 ";
$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			
			$posts = $rs-> fetch_assoc();
			return $posts;

		}else{

			echo ('This list is not exists.');

		}# end if

	}else{

			// echo ('Error executing query.') . $qry;

		}# end if

}# end postByID

// get the single post
// view-post
function postByIDSort(){

$qry = "SELECT postID, firstName, lastName, profilePic, suburbName, timeDeparture, postDescription, carSpace, timeStamp FROM usersPost, users, suburbs WHERE usersPost.userID = users.userID AND suburbs.suburbID = usersPost.suburbID ORDER BY timeDeparture ASC LIMIT 0, 5 ";
$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$posts = array();
			while($row = $rs-> fetch_assoc()){

				$posts[] = $row;

			}# end while
			
			return $posts;

		}else{

			echo ('This list is not exists.');

		}# end if

	}else{

			echo ('Error executing query.') . $qry;

		}# end if

}# end postByID

// get the comment related to the post
// in view-post
function commentByPostID($postID){
									
		$qry =	"SELECT commentID, firstName, lastName, profilePic, commentPost, timeStamp 
FROM users, comments 
WHERE users.userID = comments.userID AND postID = $postID
ORDER BY timeStamp ASC" ;
									
	$rs = $this-> db-> query($qry);

	if ($rs) {
		
		if ($rs-> num_rows > 0) {
			
			$comments = array();
			while($row = $rs-> fetch_assoc()){

				$comments[] = $row;

			}# end while
			
			return $comments;

		}else{

			$msg = ('There is no comment yet.');

		}# end if

	}else{

			echo ('Error executing query.') . $qry;

		}# end if

}# end commentByPostID

// list all the suburbs
// in navigation link
function listSuburb(){
	
	$qry = "SELECT suburbID, suburbName
					FROM suburbs
					ORDER BY suburbName ASC";

	$rs = $this-> db-> query($qry);
	if($rs){
		
		if($rs-> num_rows > 0){
			
			$suburb = array();
			while($row = $rs-> fetch_assoc()){
			
				$suburb[] = $row;
					
			}# end while
			
			return $suburb;
			
		}else{

			echo ('Suburb is not exists.');

		}# end if

	}else{

			echo ('Error executing query.') . $qry;

		}# end if
		
}# listSuburb

// get all the user by suburbID
function getUserBySuburb($sid){

	$qry = "SELECT postID, firstName, lastName, profilePic, userAccess, suburbName, timeDeparture FROM users, suburbs, usersPost WHERE users.userID = usersPost.userID AND suburbs.suburbID = usersPost.suburbID AND usersPost.suburbID = $sid ";

	$rs = $this-> db-> query($qry);
	
	if ($rs) {

	if ($rs-> num_rows > 0) {
			
			$user = array();
			while ($row = $rs-> fetch_assoc() ) {
				
				$user[] = $row;

			}# end while

			return $user;

		}else{

			$notice = ('There is no person post in this suburb at the moment');

			return $notice;

		}# end if

	}else{

			echo ('Error executing query.') . $qry;

	}# end if

}# end getUserBySuburb

// get page content
function getPageContent($page){

	$qry = "SELECT pageContentID, content FROM pages, pageContent WHERE pages.pageID = pageContent.pageID AND pageName = '$page' ";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs -> num_rows > 0){

			$content = array();
			while ($row = $rs-> fetch_assoc()) {
			$content [] = $row;
				
			}# end while

			return $content;

		}else{

			echo ('There is no content.');


		}# end if

	}else{

			echo ('Error executing query.') . $qry;

	}# end if


}# end getPageContent

// get page content by id
function getPageContentById($pid){

	$qry = "SELECT pageContentID, content FROM pages, pageContent WHERE pages.pageID = pageContent.pageID AND pageContentID = '$pid' ";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($rs -> num_rows > 0){
		
		 $content  = $rs-> fetch_assoc(); 
			return $content;

		}else{

			echo ('There is no content.');

		}# end if

	}else{

			echo ('Error executing query.') . $qry;

	}# end if


}# end getPageContent

// update content
public function updateContent(){

	if (!get_magic_quotes_gpc()) {
	
		$this-> sanitiseInput();
	
	}# end if

	extract($_POST);
	$qry = "UPDATE pageContent SET content = '$content' WHERE pageContentID = $pageContentID";

	$rs = $this-> db-> query($qry);
	
	// test if a record has been created
	if ($rs && $this-> db-> affected_rows > 0) {
	
		$msg = 'Content Updated.';
		
	}else{

		echo "Error updating content. " . $qry;
	
	}# end if

return $msg;
}# end updateContent

// update post
public function updatePost(){

	if (!get_magic_quotes_gpc()) {
	
		$this-> sanitiseInput();
	
	}# end if

	extract($_POST);
	$qry = "UPDATE usersPost SET postDescription = '$post' WHERE postID = $postID";

	$rs = $this-> db-> query($qry);
	
	// test if a record has been created
	if ($rs && $this-> db-> affected_rows > 0) {
	
		$msg = 'post Updated.';
		
	}else{

		echo "Error updating content. " . $qry;
	
	}# end if

return $msg;
}# end updatePost

// insert comment to DB
public function insertComment(){

if (!get_magic_quotes_gpc()) {
	
		$this-> sanitiseInput();
	
	}# end if

		extract($_POST);
		$qry = "INSERT INTO comments VALUES (NULL, $userID,'$commentPost', $postID, NULL) ";
		$rs = $this-> db-> query($qry);
		if ($rs && $this-> db-> affected_rows > 0) {
	
		$msg = 'Comment created.';
	
	}else{

		echo "Error inserting comment" . $qry;
	
	}# end if

	return $msg;

}# end insertComment

// delete post and comments by postID
public function deletePost(){

	$did = $_GET['did'];
	$qry = "DELETE FROM usersPost WHERE postID = $did";
	$rs = $this-> db-> query($qry);
	if ($rs) {
		
		if ($this-> db-> affected_rows > 0) {
				
			$qry1 = "DELETE FROM comments WHERE postID = $did";
			$rs1 = $this-> db-> query($qry1);
			if ($rs1) {
					
					if ($this-> db-> affected_rows > 0){

						echo "Post and Related comment deleted.";

					}# end if

			}# end if

		}# end if
	
	}else{
	
		echo 'Error executing query '.$qry;
		return false;
	
	}# end if

}# end deletePost

// delete comments by postID
public function deleteComment(){

	$did = $_GET['did'];
	
	$qry = "DELETE FROM comments WHERE commentID = $did";
	$rs = $this-> db-> query($qry);
	if ($rs) {
			
		if ($this-> db-> affected_rows > 0){

			echo "comment deleted.";

		}else{

			echo "Error Deleting comment.";

		}# end if
	
	}else{
	
		echo 'Error executing query '.$qry;
		return false;
	
	}# end if

}# end deleteComment

// insert new user
public function insertSignup(){
	if (!get_magic_quotes_gpc()) {
	
		$this-> sanitiseInput();
	
	}# end if
	extract($_POST);
	$passSHA1 = sha1($userPassword);
	$qry = "INSERT INTO users VALUES (NULL, '$userName', '$passSHA1', '$userEmail', '$firstName', '$lastName', 'people.gif', 'user', 'driver','yes')";
	$rs = $this->db->query($qry);
	if ($rs && $this->db->affected_rows > 0) {
	
		$msg = 'New user created.';
	
	}else{

		echo "Error inserting new user " . $qry;
	
	}# end if

	return $msg;

}# end insertSignup

// insert new post to db
public function insertPost(){
	if (!get_magic_quotes_gpc()) {
	
		$this-> sanitiseInput();
	
	}# end if
	extract($_POST);
	$timeDeparture = $year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.'00';
	$now = date('Y-m-d h:i:s', time());
	$qry = "INSERT INTO usersPost VALUES (NULL, $suburb, '$timeDeparture', $carSpace, '$description',$userID, '$now')";
	$rs = $this->db->query($qry);
	if ($rs && $this->db->affected_rows > 0) {
	
		$msg = 'Post created.';
	
	}else{

		echo "Error inserting post" . $qry;
	
	}# end if

	return $msg;

}# end insertPost

// sanitise character
public function sanitiseInput(){

	foreach ($_POST as &$post) {
		
		$post = $this-> db-> real_escape_string($post);

	}# end foreach

}# end sanitiseInput
	
}# end DB	
?>