<?php
include ('classes/class-db.php');

class Model extends DB {

private $validate; //  holds object for the validate class
public $loginMsg; // contain message to be displayed on the login form
public $userLoggedIn; // boolean variable which set to true if user logged in
public $adminLoggedIn; // boolean variable which set to true if user logged in
	
	function __construct(){
		
		parent::__construct();
		$pagesToValidate = array('home', 'post', 'editContent', 'createPost', 'signup', 'editPost');
		
		if(in_array($_GET['page'], $pagesToValidate)){
			
			include('classes/class-validate.php');
			$this->validate = new Validate;
			
		}# end if	
		
	}# end __construct

// check user Session
	public function checkUserSession(){
		
		// check if user logout
		if ($_GET['page'] == 'logout') {
			
			unset($_SESSION['userID']);
			unset($_SESSION['userName']);
			unset($_SESSION['firstname']);
			unset($_SESSION['lastname']);
			unset($_SESSION['userAccess']);
			$this->model->userLoggedIn = false;
			$this->model->adminLoggedIn = false;
			$this->loginMsg = 'You have succesfully logout.';
			header("Location: index.php ");

		}# end if

		// if submit button has been submitted
		if($_POST['login']){

			// check username and password
			if($_POST['userName'] && $_POST['userPassword']){
				// check if username and password match with DB, call validateUser function
				// $this-> userLoggedIn = true;
				$this->userLoggedIn = $this-> validateUser();
				$this->adminLoggedIn = $this-> validateUser();

				// if not match show the loginMsg
				if(!$this->userLoggedIn){
					
					$this->loginMsg = 'Login failed. please check your username and password.';
				
					if(!$this->adminLoggedIn){
					
					$this->loginMsg = 'Login admin failed. please check your username and password.';
					
					}# end if
				
				}else{

					// if username empty show the message
					$this->loginMsg = 'Please enter username and password.';
					
				}# end if
				
			}# end if
			
		}else{

			// if all validation is pass, set the userAccess to user
			if($_SESSION['userAccess'] == 'user'){
				// userLoggedIn to true
				$this->userLoggedIn = true;
				$this->adminLoggedIn = false;
				
			}# end if

			// if all validation is pass, set the userAccess to admin
			if($_SESSION['userAccess'] == 'admin'){
				// userLoggedIn to true
				$this->adminLoggedIn = true;
				$this->userLoggedIn = false;

			}# end if

		}# end if
		
	}# end checkUserSession

	public function validateSignup(){

		$result['firstNameMsg'] = $this->validate->checkRequired($_POST['firstName']);
		if (!$result['firstNameMsg']) {

			$result['firstNameMsg'] = $this->validate->checkRegExpName($_POST['firstName']);

		}# end if

		$result['lastNameMsg'] = $this->validate->checkRequired($_POST['lastName']);
		if (!$result['lastNameMsg']) {

			$result['lastNameMsg'] = $this->validate->checkRegExpName($_POST['lastName']);
		
			}# end if

		
		if (!$result['userNameMsg'] = $this->validate->checkRequired($_POST['userName'])) {

			if(!$result['userNameMsg'] = $this->validate->checkNotAdmin($_POST['userName'])){

				$result['userNameMsg'] = $this->validate->checkRegExpName($_POST['userName']);
			
			}# end if
		
		}# end if

		$result['userEmailMsg'] = $this->validate->checkEmail($_POST['userEmail']);
		$result['userPasswordMsg'] = $this->validate->checkRequired($_POST['userPassword']);
		$result['userPassword2Msg'] = $this->validate->checkRequired($_POST['userPassword2']);
		// check if the password match
		if (!$result['userPasswordMsg'] && !$result['userPassword2Msg']) {
			
			if (strlen($_POST['userPassword']) >= 6) {

			
			if ($_POST['userPassword'] == $_POST['userPassword2']) {

				
				$result['ok'] = $this->validate->checkErrorMessages($result);

				// $result['successMsg'] = 'You have succesfully signup, you can login now.';
				
					}else{

						$result['passwordMatchMsg'] = '*Your password does not match.';

					}# end if

				}else{

					$result['passwordMatchMsg'] = '*Your password has to be at least 6 characters.';
				
			}

		}# end if
		return $result;

		
	}# end validateSignup

	public function processSignup(){

		$vresult = $this-> validateSignup();
		if (!$vresult['ok']) {
			
			return $vresult;

		}

	$uresult = $this-> insertSignup();
	// $uresult['successMsg'] = 'You have succesfully signup, you can login now.';
	return $uresult;



	}# end processSignup

	// check user login
	private function validateUser(){
		// get all the data in users table
		$user = $this-> getUser();

		// if there is data in the $user and userAccess = user
		// set the session to match the userName and userAccess
		if (is_array($user) ) {
			
			if ($user['userAccess'] == 'user' || $user['userAccess'] == 'admin' ) {
				
				$_SESSION['userID'] = $user['userID'];
				$_SESSION['userName'] = $user['username'];
				$_SESSION['firstname'] = $user['firstname'];
				$_SESSION['lastname'] = $user['lastname'];
				$_SESSION['userAccess'] = $user['userAccess'];
				header("Location: ".$_SERVER['REQUEST_URI']." ");

			}# end if

			
			return true;
			
		}else{
			
			return false;
			
		}# end if
			
	}# end validateUser

private function validateContent($mode){
/*
echo '<pre>';	
print_r($_POST);
echo '</pre>';
*/	

	$result['contentMsg'] = $this->validate-> checkRequired($_POST['content']);

	// check if there is at least one error message in arrray error messages ($result)
	$result['ok'] = $this->validate-> checkErrorMessages($result); 

	return $result;

}# end validateContent


public function processUpdateContent(){
	// validate the product form entries

	$vresult = $this-> validateContent('Update');
	// echo "<pre>";
	// print_r($vresult);
	// echo "</pre>";
	// test if the validation result is good
	if (!$vresult['ok']) {
		
		return $vresult;
	
	}# end if

$uresult .= $this-> updateContent();
return $uresult;

}# end processUpdateContent

private function validateUpdatePost(){
/*
echo '<pre>';	
print_r($_POST);
echo '</pre>';
*/	

	$result['updateMsg'] = $this->validate-> checkRequired($_POST['post']);

	// check if there is at least one error message in arrray error messages ($result)
	$result['ok'] = $this->validate-> checkErrorMessages($result); 

	return $result;

}# end validateUpdatePost

public function processUpdatePost(){
	// validate the product form entries

	$vresult = $this-> validateUpdatePost();
	// echo "<pre>";
	// print_r($vresult);
	// echo "</pre>";
	// test if the validation result is good
	if (!$vresult['ok']) {
		
		return $vresult;
	
	}# end if

$uresult .= $this-> updatePost();
return $uresult;

}# end processUpdatePost

// validate if the comment is not empty
public function validateComment(){

	$result['commentMsg'] = $this->validate-> checkRequired($_POST['commentPost']);
	$result['ok'] = $this->validate-> checkErrorMessages($result);

	return $result;
}# end validatePage

// process before user can post a comment
public function processPostComment(){

	$vresult = $this-> validateComment();
	// echo "<pre>";
	// print_r($vresult);
	// echo "</pre>";
	if (!$vresult['ok']) {
		
		return $vresult;

	}# end if

	$uresult .= $this-> insertComment();
	return $uresult;

}# end processPostComment

public function validatePost(){
	// echo $_POST['description'];
	$result['descriptionMsg'] = $this->validate-> checkRequired($_POST['description']);
	$result['suburbMsg'] = $this->validate-> checkNotDefault($_POST['suburb']);
	$result['hourMsg'] = $this->validate-> checkNotDefault($_POST['hour']);
	$result['minuteMsg'] = $this->validate-> checkNotDefault($_POST['minute']);
	$result['carSpaceMsg'] = $this->validate-> checkNotDefault($_POST['carSpace']);
	$result['ok'] = $this->validate-> checkErrorMessages($result);
	// if($_POST['suburb'] == 0){

		// $result['suburbMsg'] = 'Please Select Suburb';
	return $result;

	// }# end if

}# end validatePost

public function processAddPost(){

	$vresult = $this-> validatePost();
	if (!$vresult['ok']) {
		
		return $vresult;

	}# end if

	if ($vresult['ok']) {
		
		$uresult = $this-> insertPost();
		$uresult['msg'] = 'Post succesfully created.';
		return $uresult;
	
	}# end if

	// extract($_POST);
	// $timeDeparture = $year.$month.$day;
	// echo $timeDeparture;
	// echo $_POST['suburb'];
	// echo $_POST['day'];
	// echo $_POST['month'];
	// echo $_POST['year'];
	// echo $_POST['carSpace'];
	// echo $_POST['description'];
	// echo $suburb;
	// echo '<pre>';
	// print_r($_POST);
	// echo '</pre>';

}# end processAddPost








}# end Model
?>