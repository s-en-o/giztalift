-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2013 at 09:17 PM
-- Server version: 5.6.13
-- PHP Version: 5.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `galDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `adminID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `userPassword` varchar(40) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  PRIMARY KEY (`adminID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminID`, `userName`, `userPassword`, `firstName`, `lastName`) VALUES
(1, 'senoamarto', '102191d007d5d29b45395e4e73f2f485bcc78271', 'Seno', 'Amarto'),
(2, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `commentPost` text NOT NULL,
  `postID` int(11) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentID`, `userID`, `commentPost`, `postID`, `timeStamp`) VALUES
(20, 8, 'No WAy!', 1, '2013-08-25 03:58:53'),
(16, 6, 'Hi karen,\r\nI''m keen to join for the ride if there is still room!\r\nCheers!', 1, '2013-08-24 13:24:29'),
(18, 3, 'this is wesborland from limp bizkit! you joker better shut up!', 1, '2013-08-24 13:57:35'),
(31, 15, 'sfdsfdsfdsfsdfdfsdfsdf', 1, '2013-08-27 20:56:13'),
(42, 25, 'hi can you give me a lift bro???', 1, '2013-09-02 22:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `pageContent`
--

CREATE TABLE IF NOT EXISTS `pageContent` (
  `pageContentID` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `pageID` int(11) NOT NULL,
  PRIMARY KEY (`pageContentID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pageContent`
--

INSERT INTO `pageContent` (`pageContentID`, `content`, `pageID`) VALUES
(1, 'This is step 1 yow!', 1),
(2, 'osdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhioposdfhodfoihsdfiohfoiiojgioj\r\nfosdfihfhfiosiojdsfgojsdfjofsdg[p\r\nhisfaoihpiohfiohoihiohpsfoihpshiosdhiop', 1),
(3, '\r\nAdvertise Â· Link to ColorPicker.com Â· Share ColorPicker.com Â· \r\n\r\nÂ© ColorPicker.com - Quick Online Color Picker Tool. HTML Color Codes	 \r\n', 1),
(4, 'Karlson', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageName` varchar(20) NOT NULL,
  `pageTitle` varchar(30) NOT NULL,
  `pageHeading` varchar(30) NOT NULL,
  `pageContent` text NOT NULL,
  `pageDescription` varchar(100) NOT NULL,
  PRIMARY KEY (`pageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pageID`, `pageName`, `pageTitle`, `pageHeading`, `pageContent`, `pageDescription`) VALUES
(1, 'home', 'Give A Lify', '', '<!-- banner -->\r\n<section id="banner">\r\n<div class="grid_6">\r\n<p>Boxing is about respect. getting it for yourself, and taking it away from the other guy. bruce... i''m god. that tall drink of water with the silver spoon up his ass. mister wayne, if you don''t want to tell me exactly what you''re doing, when i''m asked, i don''t have to lie. but don''t think of me as an idiot. are you feeling lucky punk this is the ak-47 assault.</p>\r\n<a id="join" href="#">Join now</a>\r\n</div>\r\n</section>\r\n\r\n	\r\n\r\n	', 'Connecting People'),
(2, 'post', 'Post', '', 'This is a post', 'Connecting People'),
(3, 'admin', 'Admin panel', 'Administration page', '', ''),
(4, 'logout', 'Logout ', 'Come back again!', '<!-- banner -->\r\n<section id="banner">\r\n<div class="grid_6">\r\n<p>Boxing is about respect. getting it for yourself, and taking it away from the other guy. bruce... i''m god. that tall drink of water with the silver spoon up his ass. mister wayne, if you don''t want to tell me exactly what you''re doing, when i''m asked, i don''t have to lie. but don''t think of me as an idiot. are you feeling lucky punk this is the ak-47 assault.</p>\r\n<a id="join" href="#">Join now</a>\r\n</div>\r\n</section>', ''),
(5, 'suburb', 'Browse suburbs', 'The suburbs', '', ''),
(6, 'editContent', 'Edit Content', 'Edit Content', '', ''),
(7, 'delete', 'Delete', 'Delete ', '', ''),
(8, 'createPost', 'Creating post', 'Create new post', '', ''),
(9, 'signup', 'Sign Up', 'Giz a Lift | Sign up', '', ''),
(10, 'listSuburbs', '', '', '', ''),
(11, 'profile', 'Profile', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `suburbs`
--

CREATE TABLE IF NOT EXISTS `suburbs` (
  `suburbID` int(11) NOT NULL AUTO_INCREMENT,
  `suburbName` text NOT NULL,
  PRIMARY KEY (`suburbID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `suburbs`
--

INSERT INTO `suburbs` (`suburbID`, `suburbName`) VALUES
(1, 'Hataitai'),
(2, 'Brooklyn'),
(3, 'Kilbirnie'),
(4, 'Mount Victoria'),
(5, 'Mount Cook'),
(6, 'Newtown');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(30) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `profilePic` varchar(60) CHARACTER SET utf8 NOT NULL,
  `userAccess` enum('admin','user') NOT NULL,
  `userType` enum('driver','lifter') NOT NULL,
  `userAvailability` enum('yes','no') NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `username`, `password`, `email`, `firstName`, `lastName`, `profilePic`, `userAccess`, `userType`, `userAvailability`) VALUES
(1, 'karenstoke', '7a680aee7fe93787d51d1b0ff6bbd898840af183', 'karenstoke@aaaaaa.com', 'Karen ', 'Stoke', 'people.jpg', 'user', 'driver', 'yes'),
(2, 'rachelramsay', 'f7c2340eefe56d961c659ae5fb98a94e3bbb0d30', 'rachelramsay@aaaaa.com', 'Rachel', 'Ramsay', 'people.jpg', 'user', 'driver', 'yes'),
(3, 'wesborland', '14e09bedc61e7fb16f74503a67786892b8e07868', 'wesborland@aaaaa.com', 'Wes', 'Borland', 'people.jpg', 'user', 'driver', 'yes'),
(4, 'johndoe', '6c074fa94c98638dfe3e3b74240573eb128b3d16', 'johndoe@aaaaa.com', 'John', 'Doe', 'people.jpg', 'user', 'driver', 'yes'),
(5, 'janedoe', '06d213088a72f4c1ac947c6f3d9ddd321650ebfb', 'janedoe@aaaaa.com', 'Jane', 'Doe', 'people.jpg', 'user', 'lifter', 'yes'),
(6, 'superman', '18c28604dd31094a8d69dae60f1bcd347f1afc5a', 'superman@aaaaa.com', 'Super', 'Man', 'people.jpg', 'user', 'lifter', 'no'),
(7, 'batman', '5c6d9edc3a951cda763f650235cfc41a3fc23fe8', 'batman@aaaaa.com', 'Bat', 'Man', 'people.jpg', 'user', 'driver', 'yes'),
(8, 'joker', '6c973e8803b3fbaabfb09dd916e295ed24da1d43', 'joker@aaaaa.com', 'Joker', 'is Joker', 'people.jpg', 'user', 'driver', 'yes'),
(9, 'gambit', 'cdb3fff2b74393b87188f6844b47c500fe5c9a12', 'gambit@aaaaa.com', 'Gambit', 'X Men', 'people.jpg', 'user', 'lifter', 'yes'),
(10, 'senoamarto', '102191d007d5d29b45395e4e73f2f485bcc78271', 'senoamarto@aaaaa.com', 'Seno', 'Amarto', '', 'admin', '', 'yes'),
(13, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin@aaaaaa.com', 'Admin', 'Admin', '', 'admin', 'driver', 'yes'),
(17, 'cristiano', '1ce45b474427ac72ffe24f289421a1b3fb3267c0', 'cristiano@aaaaa.com', 'Cristiano', 'Ronaldo', 'people.gif', 'user', '', ''),
(33, 'fionaaa', 'fa6977c99b809db68e1c56888ec38bd004719b39', 'fionaaaa@aaaaa.com', 'Fionaaa', 'Callaghan', 'people.gif', 'user', 'driver', 'yes'),
(32, 'jokerr', 'fa6977c99b809db68e1c56888ec38bd004719b39', 'fionaaaa@aaaaa.com', 'Fionaaa', 'Callaghan', 'people.gif', 'user', 'driver', 'yes'),
(31, 'garethbale', 'fa6977c99b809db68e1c56888ec38bd004719b39', 'fionaaaa@aaaaa.com', 'Gareth', 'Baleee', 'people.gif', 'user', 'driver', 'yes'),
(30, 'fionaa', 'c6efae9869218c6d45b92a3090bce129f27c070d', 'fionaaaa@aaaaa.com', 'Fionaaa', 'Callaghan', 'people.gif', 'user', 'driver', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `usersPost`
--

CREATE TABLE IF NOT EXISTS `usersPost` (
  `postID` int(11) NOT NULL AUTO_INCREMENT,
  `suburbID` int(11) NOT NULL,
  `timeDeparture` datetime NOT NULL,
  `carSpace` int(10) NOT NULL,
  `postDescription` text NOT NULL,
  `userID` int(11) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`postID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `usersPost`
--

INSERT INTO `usersPost` (`postID`, `suburbID`, `timeDeparture`, `carSpace`, `postDescription`, `userID`, `timeStamp`) VALUES
(1, 1, '2013-08-04 08:30:00', 3, 'Yes, i used a machine gun. i took a viagra, got stuck in me throat, i''ve had a stiff neck for hours. you want a guarantee, buy a toaster. you wouldn''t hit a man with no trousers on, would you? are you feeling lucky punk dyin'' ain''t much of a livin'', boy. at this point, i''d set you up with a chimpanzee if it''d brought you back to the world! you are as precious to me as you were to your own mother and father. i swore to them that i would protect you, and i haven''t. my lord! you''re a tripod. jasper: your baby is the miracle the whole world has been waiting for. this is my gun, clyde! your were only supposed to blow the bloody doors off.', 1, '0000-00-00 00:00:00'),
(16, 6, '2013-09-03 08:31:00', 4, 'This is a description. yes.', 25, '2013-09-02 22:04:51'),
(18, 6, '2013-09-12 12:17:00', 4, 'asdsadsadhjgkjhkjhklhjkhjkkhjkjkhkjkjhkjkjhk\r\njhkjhkjh\r\nbcv\r\nbcv\r\nbvc\r\nbvc\r\nbg\r\nggfhghfhh', 7, '2013-09-11 19:39:37'),
(19, 5, '2013-09-12 05:08:00', 6, 'fghfgjhghjghgmghjmhjmjmjh\r\nmhjmjhmjhmjhm\r\nmjhmjhmhjmjhjmhjmjhm', 6, '2013-09-11 19:45:33'),
(21, 1, '2013-09-12 04:03:00', 6, 'ASAsas', 8, '2013-09-11 20:52:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
