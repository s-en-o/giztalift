<?php
	
	class ViewHome extends View{
		
		private $usersAsc;
		private $content;
		private $posts;

	 protected function content(){
			
		$html = $this->pageInfo['pageContent'];
	
			$this->usersAsc = $this->model->showUserAsc();
			$this->content = $this->model->getPageContent($_GET['page']);
			$this->posts = $this->model->postByIDSort($_GET['postID']);
			// echo '<pre>';
			// print_r($_SESSION);
			// echo '</pre>';

			$html .= '<!-- walkthrough -->

	<section id="walkthrough">


		<h2>4 simple steps</h2>';

		for ($i=0; $i < count($this->content) ; $i++) { 

		$html .= '
		<div class="grid_4">

			<div class="imgCircle'.$i.'"></div>
			<div id="box-top"></div>
			
			<p contenteditable="true">
			'.$this->content[$i]['content'].'
			</p>
			
		<div id="box-bottom"></div>
		';

		if ($this->model->adminLoggedIn) {
			
			$html .= '<a class="editLink" href="index.php?page=editContent&amp;pid='.$this-> content[$i]['pageContentID'].'">edit</a>';
		
		}# end if
		
		$html .= '</div>';
			
		}# end for

		$html .= '</section>';

		// content section
		$html .= '<!-- list lifter -->
							<section id="lifter">
							';

		$html .= $this->displayUsers();

		$html .= '</section>';

return $html;

}# end content
		
	private function displayUsers(){

		$html = '';

		$html .= '
	<div class="grid_8">

	<h2>leaving soon</h2>';
	if (is_array($this->posts)) {

	foreach ($this->posts as $post) {
		
	$timeDeparture = explode(" ", $post['timeDeparture']);

	$html .= '

	<div class="profile">
				
	<div class="profilePic">
		<img src="images/'.$post['profilePic'].'" alt="'.$post['firstName']. ' ' .$post['lastName'].'">
	</div>
				
	<div class="detail">
	
		<p class="profileName">'.$post['firstName']. ' ' .$post['lastName'].'</p>
		<p class="schedule">Driving from '.$post['suburbName'].', '
		// convert timestamp from to unix time
		.date('d F Y', strtotime($timeDeparture[0])).' on '.date('h:i a', strtotime($timeDeparture[1])).

		'.</p>
	
	</div>
				
		<div class="goWrap">
			<a class="go" href="index.php?page=post&amp;postID='.$post['postID'].'">go</a>
		</div>';

		if ($this->model->adminLoggedIn) {

			$html .= '<div class="parent-deletePost"><a class="link-deletePost" href="index.php?page=delete&amp;src=post&amp;did=' .$post['postID']. '" onclick="return confirm(\'Are you sure?\');">Delete</a></div>';

		}# end if

		$html .= '</div><!-- 	END PROFILE -->';

	}# end foreach

	}else{

		$html .= '<h2>Sorry there is no one leaving at the moment.</h2>'."\n";

	}# end if
	$html .= '
		</div>
	<!-- end leave soon -->
	
	<div class="grid_8">

	<h2>new lifter</h2>';

	if (is_array($this->usersAsc)) {
// get user ASC
foreach ($this->usersAsc as $usersAscs) {

	$timeDeparture = explode(" ", $usersAscs['timeDeparture']);
	
	$html .= '

	<div class="profile">
				
	<div class="profilePic">
		<img src="images/'.$usersAscs['profilePic'].'" alt="'.$usersAscs['firstName']. ' ' .$usersAscs['lastName']. '">
	</div>
				
	<div class="detail">
	
		<p class="profileName">'.$usersAscs['firstName']. ' ' .$usersAscs['lastName']. '</p>
		<p class="schedule">Driving from '.$usersAscs['suburbName'].', ';

		$html .= ' '.date('d F Y', strtotime($timeDeparture[0])).' on '.date('h:i a', strtotime($timeDeparture[1])).'.</p>
	
	</div>
				
		<div class="goWrap">
			<a class="go" href="index.php?page=post&amp;postID='.$usersAscs['postID'].'">go</a>
		</div>';

		if ($this->model->adminLoggedIn) {

			$html .= '<div class="parent-deletePost"><a class="link-deletePost" href="index.php?page=delete&amp;src=post&amp;did=' .$post['postID']. '" onclick="return confirm(\'Are you sure?\');">Delete</a></div>';

		}# end if											
				
		$html .= '</div><!-- 	END PROFILE -->';
					
}# end foreach

}else{

		$html .= '<h2>Sorry there is no one leaving at the moment.</h2>'."\n";

}# end if
$html .= '</div><!-- end new lifter -->';

return $html;
}# end displayUsers
	

			
	
		
		
}# end ViewHome	
?>