<?php
// viewing specific post
class ViewPost extends View{

private $post;
private $comments;


protected function content(){

	$this-> post = $this-> model-> postByID($_GET['postID']);
	$this-> comments = $this-> model-> commentByPostID($_GET['postID']);

	// echo '<pre>';
	// print_r($this-> post);
	// print_r($_SESSION);
	// echo '</pre>';

	$html = '<!-- banner -->
	<section id="banner">

	<div class="grid_6">

	<p>Boxing is about respect. getting it for yourself, and taking it away from the other guy. bruce... i\'m god. that tall drink of water with the silver spoon up his ass. mister wayne, if you don\'t want to tell me exactly what you\'re doing, when i\'m asked, i don\'t have to lie. but don\'t think of me as an idiot. are you feeling lucky punk this is the ak-47 assault.</p>

	<a id="join" href="#">Join now</a>

	</div>

	</section>';
	$timeDeparture = explode(" ", $this-> post['timeDeparture']);
	$timeStamp = explode(" ", $this-> post['timeStamp']);
	$html .= '<section id="lifter">'; 
	$html .= '<div id="post-wrap">'; 
	$html .= '<div id="timeStamp"><span>post updated '.date('d F Y', strtotime($timeStamp[0])).' at '.date('h:i a', strtotime($timeStamp[1])).'</span></div>';
	$html .= '<div id="post-avatar"><img src="images/people.jpg"></div>';
	$html .= '<div id="post-insideWrap">';
	$html .= '<div id="post-author">'.$this-> post['firstName'].' '.$this-> post['lastName'].'</div>'."\n";
	$html .= '<div id="post-trip">';
	$html .= '<span id="post-map">'.$this-> post['suburbName'].'</span>'."\n";
	$html .= '<span id="post-time">'.date('d F Y', strtotime($timeDeparture[0])).' at '.date('h:i a', strtotime($timeDeparture[1])).'</span>'."\n";
	$html .= '<span id="post-carspace">'.$this-> post['carSpace'].'</span>'."\n";
	$html .= '</div>';
	$html .= '</div>';
	$html .= '<div id="post-detail">'.$this-> post['postDescription'];
	if ($_SESSION['userID'] == $this-> post['userID']) {
		$html .= '<div id="editPost-wrap"><a href="index.php?page=editPost&amp;eid='.$_GET['postID'].'">Edit Post</a></div>';
	}# end if
	$html .= '</div>';
	$html .= '</div>';
	$html .= '<span id="strip"></span>'; 

	if (isset($_POST['post'])) {

	$result = $this-> model-> processPostComment();
	// echo "<pre>";
	// print_r($result);
	// echo "</pre>";
	if ($result['ok']) {

	header("Location: ".$_SERVER['REQUEST_URI']." ");

	}# end if

	}# end if

	// if user or admin is logged in show form
	if ($this->model->userLoggedIn || $this->model->adminLoggedIn) {

		// check if there is any comment for the specific comment
		if (!is_array($this-> comments)) {
			
			$html .= '<br>There is no comment yet.';

		}else{
			$html .= '<div id="comment-wrap">';
			$html .= '<div id="comment-count">'.count($this-> comments).' Comment';
			if(count($this-> comments) > 1){

				$html .='s';
				
			}# end if
			$html .= '</div>';
			foreach ($this-> comments as $comment) {

				$timeStamp = explode(" ", $comment['timeStamp']);
				$html .= '<div class="comment-box">';
				$html .= '<div class="comment-avatar"><img src="images/people.jpg"></div>';
				$html .= '<div class="comment-author">';
				$html .= '<div class="comment-name">'.$comment['firstName']. ' ' . $comment['lastName'] .'</div>';
				$html .= '<div class="comment-date">'.'<p>'.date('d F Y', strtotime($timeStamp[0])). ' at ' .date('h:i a', strtotime($timeStamp[1])).'</p>'.'</div>';
				$html .= '</div>';
				$html .= '<div class="comment-text"><p>'.$comment['commentPost'].'</p></div>'."\n";


				if ($this->model->adminLoggedIn) {

					$html .= '<div class="parent-delete"><a class="link-delete" href="index.php?page=delete&amp;src=comment&amp;postID='.$_GET['postID'].'&amp;did=' .$comment['commentID']. '" onclick="return confirm(\'Are you sure?\');">Delete</a></div>';


				}elseif ($this->model->userLoggedIn) {

					$html .= '';

				}# end if
				$html .= '</div>';
			}# end foreach
			$html .= '</div>';
		}# end if
			$html .= $this-> editForm($result);

		// call the form function add to the page

	}else{

		$html .= '<div id="signup-login"><p>Please <a href="index.php?page=signup">Signup</a> or <a class="login" href="#">Login</a> to comment</p></div>';

	}#	end if

	$html .= '</section>'; 


	return $html;
}# end content

// the form
protected function editForm( $result ){
	if (is_array($result)) {

		extract($result);

	}# end if

	$html .= '<form id="form-comment" method="post" action="'.htmlentities($_SERVER['REQUEST_URI']).'">';
	$html .= '<input type="hidden" name="userID" value="'.$_SESSION['userID'].'">';
	$html .= '<input type="hidden" name="postID" value="'.$_GET['postID'].'">';
	$html .= '<label for="commentPost">Comment<span>*</span></label>';
	$html .= '<textarea id="commentPost" name="commentPost"></textarea>';
	$html .= '<span>'.$commentMsg.'</span>';
	$html .= '<div id="comment-btn"><input type="submit" name="post" value="Post comment"></div>';

	$html .= '</form>';

	return $html;

}# commentForm






}# end ViewPost
?>