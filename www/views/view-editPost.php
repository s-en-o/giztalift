<?php

class ViewEditPost extends View{
private $post;

protected function content(){

	$this->post = $this->model->postByID($_GET['eid']);

	// echo "<pre>";
	// print_r($_GET['eid']);
	// print_r($this->post);
	// echo "</pre>";

	if ($_POST['update']) {
		
		$result = $this->model->processUpdatePost();
		if ($result) {
			header('Location: index.php?page=post&postID='.$_GET['eid'].'');
		}
	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";

	}# end if

	$html = '<section id="lifter">';
	if (!$this->model->userLoggedIn) {

			$html .= '<p>This page is restricted. <a href="index.php">Lost?</a></p>';
			return $html;
		
		}# end if 
	$html .= $result;
	$html .= '<form method="post" action"'.htmlentities($_SERVER['REQUEST_URI']).'">';
	$html .= '<input type="hidden" name="postID" value="'.$_GET['eid'].'">';
	$html .= '<textarea name="post">'.$this->post['postDescription'].'</textarea>';
	$html .= '<div>'.$updateMsg.'</div>';
	$html .= '<input type="submit" name="update" value="Update">';
	$html .= '</form>';
	$html .= '</section>'; 
	return $html;
}# end content

}# end ViewEditPost

?>