<?php

abstract class View{
	
	protected $pageInfo;
	protected $model;
	
	public function __construct($info, $model){
		
		$this->pageInfo = $info;
		$this->model = $model;
		
	}# __construct 
	
/* compile all the function in to one */
	public function page(){
		
		$this->model->checkUserSession();
		$html = $this-> header();
		$html .= $this-> content();
		$html .= $this-> footer();

		
		return $html;	
	}# end page

/* header html */
	private function header(){
		// date_default_timezone_set('Pacific/Auckland');
		$suburbs;
		
		$this->suburbs = $this->model->listSuburb();
		

		// echo '<pre>';
		// print_r($this-> suburbs);
		// echo '</pre>';



		$html = '<!DOCTYPE html>';
		$html .= '<html>';
		$html .= '<head>';
		$html .= '<title>' . $this->pageInfo['pageTitle'] . '</title>';
		$html .= '<meta charset="utf-8" />';
		$html .= '<meta name="description" content="'.$this->pageInfo['pageDescription'].'" />';
		$html .= '<link rel="stylesheet" href="css/main.css" />';
		$html .= '</head>';
		$html .= '<body>';

		$html .= '<div id="container" class="container_16">';
		$html .= '<!--signup form-->'."\n";
		if (!$this->model->userLoggedIn) {
			
			$html .= '<div id="dialog" title="Please signup or login">
  <p>You have to sign up or login, before you can post.</p>
		</div>
';
	
	}# end if
		
		$html .= '<!--login form-->'."\n";
		$html .= '<section id="loginForm">'."\n";
		$html .= '<form method="post" action="#">'."\n";
		$html .= '<input type="text" name="userName" id="userName" placeholder="username" required>'."\n";
		$html .= '<input type="password" name="userPassword" id="userPassword" placeholder="password" required>'."\n";
		$html .= '<input type="submit" name="login" id="submit" value="login">'."\n";
		$html .= '</form>'."\n";
		$html .= '<a id="closeForm" href="#">close</a>'."\n";
		$html .= '<p>'.$this->model->loginMsg.'</p>'."\n";
		$html .= '</section>';

		$html .= '<!-- header -->
	<section id="header">';
	
	$html .= '
		<header><div id="header-wrap">';

		if($this->model->userLoggedIn || $this->model->adminLoggedIn){
		
			$html .= '<p>Welcome, <span id="fullname">'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'</span> | <span id="links-header"><a id="link-profile" href="index.php?page=profile">Profile</a> | <a id="link-logout" href="index.php?page=logout">Logout</a></span></p>';
		
		}else{

			$html .= '<p><a id="signup" href="index.php?page=signup">Sign up</a> or <a class="login header" href="#">login</a></p>'."\n";

		}# end if


			$html .='			
			<a class="logo" href="#">Logo</a>
			</div>
			<nav>
				
				<ul>
					<li><a href="index.php">home</a></li>
					<li id="browse-li"><a id="browse" href="#">suburbs</a>
				
				<ul>';
						
		foreach($this-> suburbs as $suburb){
			
			$html .= '<li><a href="index.php?page=suburb&amp;sid='. $suburb['suburbID'] .'">'. $suburb['suburbName'] .'</a></li>';
				
		}# end foreach
		
							
		$html .= '</ul></li>';
		// switch navigation if user hasn't sign up or login 
		$html .= '<li><a id="post"'; 
		if ($this->model->userLoggedIn) {

			$html .= 'href="index.php?page=createPost"'; 
		
		}else{
			
			$html .= 'href="#"'; 

		}# end if

		$html .= '>Post</a></li>';

		$html .= '
				</ul>

			<form method="post" action="#">	
				<input type="text" id="search" placeholder="search.."/>
			</form>
			</nav>

		</header>
			<div id="update"></div>
	</section>';
		
		return $html;
	}# end header

/* content every page */
	abstract protected function content();
	# end content

/* footer */
	private function footer(){
		
		$html = '
<section id="footer">
<div id="seps" class="grid_16"></div>

	<footer>
	<div class="grid_5">
		<a class="logo" href="#">Logo</a>

</div>		

	<div id="sitemap" class="grid_6">
			<p>&copy;Copyright GiveALify 2013. All Right Reserved.
		Please read our <a id="terms" href="#">Terms &amp; Policy</a>.</p>
		<!--<a href="#">home</a><a href="#">about us</a><a href="#">browse</a><a href="#">contact us</a>-->
	</div>		

		<div id="social" class="grid_5">

			<a id="twitter" href="#">twitter</a>
			<a id="facebook" href="#">facebook</a>
						
	</div>
	
	</footer>
</section>	
	</div>
';
		$html .= '<script src="js/jquery-1.9.1.js"></script>';
		$html .= '<script src="js/jquery-ui.js"></script>';
		$html .= '<script src="js/main.js"></script>';
		if (!$this->model->userLoggedIn) {
			
			$html .= '<script src="js/modal.js"></script>';

		}# end if
		$html .= '</body>'."\n".'</html>';
		return $html;
		
	}# end footer
	

}# end View 
?>