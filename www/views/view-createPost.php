<?php
// page to create a post 
class ViewCreatePost extends View{
	private $suburb;

	protected function content(){

		$this-> suburbs = $this-> model-> listSuburb();
		$html = '<h2>'.$this-> pageInfo['pageHeading'].'</h2>';
		// echo '<pre>';
		// print_r($_SESSION);
		// echo '</pre>';
		// echo strlen(12);
		// echo count(date('Y'));
		// validate the page
		// echo time();
		// echo date('Y-m-d h:i:s', time());
		$html .= '<section>';
		if (!$this->model->userLoggedIn) {

			$html .= '<p>This page is restricted. <a href="index.php">Lost?</a></p>';
			return $html;
		
		}# end if
		// else{

			if ($_POST['submit']) {
				
				$result = $this->model->processAddPost();

/*
				echo "<pre>";
				print_r($result);
				echo "</pre>";
*/

				// echo $_POST['suburb'];
				// echo $_POST['day'];
				// echo $_POST['month'];
				// echo $_POST['year'];
				// echo $_POST['hour'];
				// echo $_POST['minute'];
				// echo $_POST['carSpace'];
				// echo $_POST['description'];

			}# end if

			$html .= $this->createPostForm($result);
			$html .= '</section>';
		// }# end if

		return $html;
	}# end content

	private function createPostForm($result){
		if (is_array($result)) {

			extract($result);
		
		}# end if
		$html = '<div>'.$msg.'</div>';
		$html .= '<form id="postForm" method="post" action="'.htmlentities($_SERVER['REQUEST_URI']).'">';
		$html .= '<input type="hidden" name="userID" value="'.$_SESSION['userID'].'">';
		$html .= $this-> suburbList($suburbMsg);	

		$day = date('d');
		$html .= $this-> dayDropdown($name, $day); 
	
		$month = date('m');
		$html .= $this-> monthDropdown($name, $month);

		$month = date('Y');
		$html .= $this-> yearDropdown($name, $month);
		$html .= $this->hourDropDown($hourMsg);
		$html .= $this->minuteDropDown($minuteMsg);
		$html .= $this->carSpaceDropDown($carSpaceMsg);
		$html .= $this->description($descriptionMsg);
		
		$html .= '<input type="submit" name="submit" value="Submit">';
		$html .= '</form>';
		return $html;

	}# end createPostForm

	private function suburbList($suburbMsg){
		$html .= '<div id="select-suburbWrap">';
		$html .= '<label for="suburb">Suburb</label>';
		$html .= '<select id="select-suburb" name="suburb">';
		$html .= '<option value="0">-Please Select Suburbs-</option>';
		foreach ($this-> suburbs as $suburb) {
		
			$html .= '<option'; 
			if ($_POST['suburb'] == $suburb['suburbID']) {
				$html .= ' selected="selected" ';
			}# end if
			$html .= ' value="'.$suburb['suburbID'].'">'.$suburb['suburbName'].'</option>';

		}# end foreach
		$html .= '</select>';
		$html .= '<span>'.$suburbMsg.'</span>';
		$html .= '</div>';
		return $html;;
	}# end suburbList

	// day drop down 
	private function dayDropdown($selected=null){

		$html = '<div>';
		$html .= '<label for="day">Day</label>';
		$html .= '<select name="day" id="day">';

   /*** the current day ***/
   $selected = is_null($selected) ? date('d', time()) : $selected;

   for ($i = 1; $i <= 31; $i++)
   {
     $html .= '<option value="'.$i.'"';
     if ($i == $selected)
     {
             $html .= ' selected';
     }
     /*** get the day ***/
     $html .= '>'.$i.'</option>';
   }
   $html .= '</select>';
   return $html;

}# end dayDropdown

// month drop down 
	private function monthDropdown($selected=null){

  $html = '<select name="month" id="month">';

  /*** the current month ***/
  $selected = is_null($selected) ? date('n', time()) : $selected;

  for ($i = 1; $i <= 12; $i++)
  {
    $html .= '<option value="'.$i.'"';
    if ($i == $selected)
    {
            $html .= ' selected';
    }
    /*** get the month ***/
    $mon = date("F", mktime(0, 0, 0, $i+1, 0, 0, 0));
    $html .= '>'.$mon.'</option>';
  }
  $html .= '</select>';
  return $html;
	
	}# end monthDropDown

// year drop down 
private function yearDropdown($selected=null){

		$html = '<select name="year" id="year">';

   /*** the current day ***/
   $selected = is_null($selected) ? date('Y', time()) : $selected;

   for ($i = date('Y'); $i <= date('Y'); $i++)
   {
     $html .= '<option value="'.$i.'"';
     if ($i == $selected)
     {
             $html .= ' selected';
     }
     /*** get the day ***/
     $html .= '>'.$i.'</option>';
   }
   $html .= '</select>';
   $html .= '</div>';
   return $html;


}# end dayDropdown

private function hourDropDown($hourMsg){
	$html = '<div>';
	$html .= '<label for="time">Time</label>';
	$html .= '<select name="hour">';
	$html .= '<option value="0">Hour</option>';
	for ($i = 1; $i <= 24 ; $i++) { 
		
		if (strlen($i) == 1) {
			$i = '0'.$i;
		}# end if

		$html .= '<option ';
			if ($_POST['hour'] == $i) {
				$html .= 'selected="selected" ';
			}# end if
		$html .= 'value="'.$i.'">'.$i.'</option>';
	
	}# end for
	$html .= '</select>';
	$html .= '<span>'.$hourMsg.'</span>';
	return $html;
}

private function minuteDropDown($minuteMsg){
		$html .= '<select name="minute">';
		$html .= '<option value="0">Minute</option>';

		for ($i = 1; $i <= 60 ; $i++) { 
			
			if (strlen($i) == 1) {
				$i = '0'.$i;
			}# end if

			$html .= '<option ';
			if ($_POST['minute'] == $i) {
					$html .= 'selected="selected" ';
				}# end if 
			$html .= 'value="'.$i.'">'.$i.'</option>';
		
		}# end for
		$html .= '</select>';
	$html .= '<span>'.$minuteMsg.'</span>';
		$html .= '</div>';
		return $html;
}

private function carSpaceDropDown($carSpaceMsg){
	$html = '<div>';
	$html .= '<label for="carSpace">Car space</label>';
	$html .= '<select name="carSpace">';
	$html .= '<option value="0">-How many seat space?-</option>';
	for ($i=1; $i<=7; $i++) { 
	 
		$html .= '<option ';
			if ($_POST['carSpace'] == $i) {
				$html .= 'selected="selected" ';
			}# end if
		$html .= ' value="'.$i.'">'.$i.'</option>';

	}# end for
	$html .= '</select>';
	$html .= '<span>'.$carSpaceMsg.'</span>';
	$html .= '</div>';
	return $html;
}

private function description($descriptionMsg){
	$html = '<div>';
	$html .= '<label>Description</label>';
	$html .= '<textarea name="description">'.$_POST['description'].'</textarea>';
	$html .= '<span>'.$descriptionMsg.'</span>';
	$html .= '</div>';
	return $html;
}

	
}# end ViewCreatePost
?>