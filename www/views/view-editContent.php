<?php

class ViewEditContent extends View{

	protected function content(){
		$html = '<section>';
		$html .= '<h2>'.$this-> pageInfo['pageHeading'].'</h2>';
	 	// echo "<pre>";
		 // print_r($msg);
		 // echo "</pre>";

		if(!$this->model->adminLoggedIn){
			
			$html .= 'This page is restricted. <a href="index.php?page=home">Lost?</a>';
			return $html;
			
		}# end if
		
		if ($_POST['Update']) {

	 	$result = $this-> model-> processUpdateContent();
	 	$content = $_POST;
	 // 	echo "<pre>";
		//  print_r($result);
		//  echo "</pre>";

	 }else{

		 $content = $this-> model-> getPageContentById($_GET['pid']);
			// echo "<pre>";
		 // print_r($content);
		 // echo "</pre>";
	 
	 }# end if		
		
	$html .= $this-> editForm('Update', $result, $content);

	$html .= '</section>';

	return $html;
	}# end content

	// display form
	protected function editForm($mode, $result, $content){
		
		if (is_array($result)) {

			extract($result);
		
		}# end if
	 extract($content);

		$html .= '<div>'.$msg.'</div>';
		$html .= '<form id="updateForm" method="post" action="'.htmlentities($_SERVER['REQUEST_URI']).'">';
		$html .='<input type="hidden" name="pageContentID" value="'.$pageContentID.'" />'."\n";
		$html .= '<div>';
		$html .= '<textarea name="content">'.htmlentities(stripslashes($content), ENT_QUOTES).'</textarea>';
		$html .= '</div>';
		$html .= '<div id="contentMsg" class="error"> '.$contentMsg.'</div>'."\n";
		$html .= '<input type="submit" name="'.$mode.'" value="Update">';
		$html .= '<a href="index.php">Cancel</a>';
		 
		$html .= '</form>';
		return $html;
	}# end editForm
	
}# end ViewEditContent
?>