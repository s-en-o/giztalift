<?php

class ViewAdmin extends View{

	protected function content(){

		$html = '<!--login form-->'."\n";
		$html .= '<section id="loginForm">'."\n";
		$html .= '<form method="post" action="'.htmlentities($_SERVER['REQUEST_URI']).'">'."\n";
		$html .= '<input type="text" name="username" id="username" placeholder="username" required>'."\n";
		$html .= '<input type="password" name="password" id="password" placeholder="password" required>'."\n";
		$html .= '<input type="submit" name="submit" id="submit" value="login">'."\n";
		$html .= '</form>'."\n";
		$html .= '<a id="closeForm" href="#">close</a>'."\n";
		$html .= '</section>';

		return $html;
	}# end content

}# end ViewAdmin

?>