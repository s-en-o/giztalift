<?php

class ViewSuburb extends View{

private $userSuburb;

protected function content(){

	$html = $this-> pageInfo['pageContent'];

	$this-> userSuburb = $this-> model-> getUserBySuburb($_GET['sid']);

	// echo '<pre>';
	// print_r($this-> userSuburb);
	// echo '</pre>';

		$html .= '<!-- list lifter -->'."\n".'<section id="lifter">';
		
		if (is_array($this-> userSuburb)) {
			
			foreach ($this-> userSuburb as $us) {
			
				$html .= '<div class="profile">
					
		<div class="profilePic">
			<img src="images/'.$us['profilePic'].'" alt="'.$us['firstName']. '' .$us['lastName'].'">
		</div>
					
		<div class="detail">
		
			<p class="profileName">'.$us['firstName']. ' ' .$us['lastName'].'</p>
			<p class="schedule">Driving from '.$us['suburbName'].', '.$us['timeDeparture'].'.</p>
		
		</div>
					
			<div class="goWrap">
				<a class="go" href="index.php?page=post&amp;postID='.$us['postID'].'">go</a>
			</div>											
					
			</div><!-- 	END PROFILE -->';

			}# end foreach
	
		}else{

			$html .= '<p>There is no post on this suburb yet</p>';

		}# end if



		$html .= '</section>';

	return $html; 

}# end content

}# end ViewBrowse
?>