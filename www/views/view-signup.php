<?php 

class ViewSignup extends View{

	protected function content(){

		$html = '<section>';
		$html .= '<h2>'.$this->pageInfo['pageHeading'].'</h2>';

		if ($_POST['signup']) {
		 
	 	$result = $this->model->processSignup();

			// echo $_POST['firstName'];
			// echo $_POST['lastName'];
			// echo $_POST['userName'];
			// echo $_POST['userEmail'];
			// echo $_POST['userPassword'];
			// echo $_POST['userPassword2'];
			echo count($_POST['userPassword']);

		}# end if 

		$html .= $this-> signupForm($result);
		$html .= '</section>';
		return $html;
	}# end content

	public function signupForm($result){
		if (is_array($result)) {

			extract($result);

		}# end if
		$html .= '<span>'.$successMsg.'</span>';
		$html .= '<form id="signup-form" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
		$html .= '<div>';
		$html .= '<span>'.$firstNameMsg.'</span>';
		$html .= '<input type="text" name="firstName" value="'.$_POST['firstName'].'" placeholder="firstname">';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$lastNameMsg.'</span>';
		$html .= '<input type="text" name="lastName" value="'.$_POST['lastName'].'" placeholder="lastname">';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$userNameMsg.'</span>';
		$html .= '<input type="text" name="userName" value="'.$_POST['userName'].'" placeholder="username">';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$userEmailMsg.'</span>';
		$html .= '<input type="text" name="userEmail" value="'.$_POST['userEmail'].'" placeholder="email">';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$passwordMatchMsg.'</span>';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$userPasswordMsg.'</span>';
		$html .= '<input type="password" name="userPassword" placeholder="password">';
		$html .= '</div>';
		$html .= '<div>';
		$html .= '<span>'.$userPassword2Msg.'</span>';
		$html .= '<input type="password" name="userPassword2" placeholder="re-type password">';
		$html .= '</div>';
		$html .= '<input type="submit" name="signup">';
		$html .= '</form>';
		
		return $html;

	}# end signupForm
	








}# end ViewSignup
?>