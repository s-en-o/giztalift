<?php

class ViewProfile extends View{
private $user;
	protected function content(){
		$this->user = $this->model->getUserByID($_SESSION['userID']);

		// echo "<pre>";
		// print_r($this->user);
		// echo "</pre>";

		$html = '<section>';
		if (!$this->model->userLoggedIn) {

			$html .= '<p>This page is restricted. <a href="index.php">Lost?</a></p>';
			return $html;
		
		}# end if
		$html .= '<form id="profileForm" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
		$html .= '<img src="images/people.gif">';
		$html .= '<input type="file" name="productImage" />';
		$html .= '<div>';
		$html .= '<label>Firstname</label>';
		$html .= '<input type="text" name="firstName" value="'.$this->user['firstname'].'" disabled="disabled">';
		$html .= '</div>';
		
		$html .= '<div>';
		$html .= '<label>Lastname</label>';
		$html .= '<input type="text" name="lastName" value="'.$this->user['lastname'].'" disabled="disabled">';
		$html .= '</div>';
		
		$html .= '<div>';
		$html .= '<label>Username</label>';
		$html .= '<input type="text" name="userName" value="'.$this->user['username'].'">';
		$html .= '</div>';
		
		$html .= '<div>';
		$html .= '<label>Password</label>';
		$html .= '<input type="password" name="password">';
		$html .= '</div>';
		
		$html .= '<div>';
		$html .= '<label>Password</label>';
		$html .= '<input type="password" name="password1">';
		$html .= '</div>';
		
		$html .= '<input type="submit" name="update" value="Update">';
		$html .= '</form>';

		$html .= '</section>';

		return $html;

	}# end content

}# end ViewProfile
?>