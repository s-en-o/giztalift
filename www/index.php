<?php
/* !Index */
session_start();
include('views/class-view.php');
include('classes/class-model.php');

class pageSelector{
	
	public function run(){
		
		if(!$_GET['page']){
			
    $_GET['page'] = 'home';
			
		}# end if
		
		// established connection to DB
		$model = new Model;
		// echo "<pre>";
		// print_r($this-> userLoggedIn);
		// echo "</pre>";
		// get the query from DB whatever the page is
		$pageInfo = $model-> pageInfo($_GET['page']);
		
		switch($_GET['page']){
			// home and logout page
			case 'logout':
			case 'home':			
			
			include('views/view-home.php');

			$view = new ViewHome($pageInfo, $model);
			
			break;
			
			// post page
			case 'signup':
			
			include('views/view-signup.php');
			$view = new ViewSignup($pageInfo, $model);

			break;
			
			// post page
			case 'post':
			
			include('views/view-post.php');
			$view = new ViewPost($pageInfo, $model);
			
			break;

			// admin page
			case 'admin':

			include('views/view-admin.php');
			$view = new ViewAdmin($pageInfo, $model);
			
			break;

			// suburb page
			case 'suburb':
			
			include('views/view-suburb.php');
			$view = new ViewSuburb($pageInfo, $model);
			
			break;
			
			// edit content page
			case 'editContent':
			
			include('views/view-editContent.php');
			$view = new ViewEditContent($pageInfo, $model);
			
			break;

			// delete post
			case 'delete':
			
			include('views/view-delete.php');
			$view = new ViewDelete($pageInfo, $model);
			
			break;

			// delete post
			case 'createPost':
			
			include('views/view-createPost.php');
			$view = new ViewCreatePost($pageInfo, $model);
			
			break;

			// list suburbs for search
			case 'listSuburbs':
			
			include('views/view-listSuburbs.php');
			$view = new ViewlistSuburbs($pageInfo, $model);
			
			break;

			// list suburbs for search
			case 'editPost':
			
			include('views/view-editPost.php');
			$view = new ViewEditPost($pageInfo, $model);
			
			break;

			// list suburbs for search
			case 'profile':
			
			include('views/view-profile.php');
			$view = new ViewProfile($pageInfo, $model);
			
			break;

		}# end switch
			
		echo $view-> page();
	}# end run
	
	
}# end pageSelector	

// display the html
$pageSelector = new pageSelector;
$pageSelector-> run();
?>